import time
from music21 import converter
import glob
import numpy as np
from numpy import * 
from convertMidi import streamToNoteArray
import scipy.misc
from PIL import Image

import os
import fnmatch

path = 'midi_database'

midi_files = [os.path.join(dirpath, f)
    for dirpath, dirnames, files in os.walk(path)
    for f in fnmatch.filter(files, '*.mid')]

musicIndex = 1
convertIndex = 0
print "Total number of musics: " + str(len(midi_files))
for f in midi_files[:10000]:

    start = time.clock()
    try:
        s = converter.parse(f)
    except Exception, e:
    	print("Exception")
    	print e
        continue

    # print("Convertendo " + str(count) + "/100")
    arr = streamToNoteArray(s.parts[0]) # just extract first voice

    musicMatrix = np.array([[0 for i in range(128)] for j in range(128)])

    print "Music size: ", len(arr)
    convertedNotes = 0

    firstSoundNoteIndex = 0
    for note in arr:
        if note < 128:
            break
        firstSoundNoteIndex += 1

    for i in range(firstSoundNoteIndex, len(arr)):
    	if arr[i] < 128:
            for j in range(0, arr[i]):
                musicMatrix[j][convertedNotes] = 1
            # musicMatrix[arr[i] - 1][convertedNotes] = 1
            
        convertedNotes += 1
        if convertedNotes == 128: 
            break

    print("Converted " + str(musicIndex) + ":", f, "it took", time.clock() - start)

    rgbArray = np.zeros((128, 128, 3), 'uint8')
    rgbArray[..., 0] = musicMatrix*255
    rgbArray[..., 1] = musicMatrix*255
    rgbArray[..., 2] = musicMatrix*255

    img = Image.fromarray(rgbArray)
    img.save('convertedImages/myimgColored' + str(convertIndex)+ '.jpg')

    # scipy.misc.imsave('testingImage' + str(convertIndex) + '.jpg', musicMatrix)
    convertIndex += 1
    musicIndex += 1
# np.savez('melody_training_dataset.npz', train=training_dataset)